"""
'event-search-right' is the class name for the View Details buttons href

The next page query param is '?p=2' with 2 being whatever number for the page

Each page shows 10 events out of 2831 total -- 284 pages total

Columns for csv:
- Company
- Show
- Date Start
- Date End
- Attendees
- Exhibitors

"""


from bs4 import BeautifulSoup
from urllib2 import urlopen
from datetime import datetime
import dateutil.parser
import re
import csv

base = 'https://eventegg.com/tradeshows/'
nextpage = '?p='
page = 1

fieldnames = ["Companies", "Shows", "Starts", "Ends", "Attendees", "Exhibitors"]

def convert(input):
    if isinstance(input, dict):
        return {convert(key): convert(value) for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [convert(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def get_event_info(event_url):
	openurl = urlopen(event_url)
	soup = BeautifulSoup(openurl, 'html.parser')

	# Company Name
	company = soup.find(itemprop="alternateName").get_text()

	# Show Name
	show = soup.find('span', class_='event-fn').get_text()

	# Start Date
	starts = dateutil.parser.parse(soup.find(itemprop="startDate")['content'])
	starts = datetime.strftime(starts,'%m/%d/%Y')

	# End Date
	ends = dateutil.parser.parse(soup.find(itemprop="endDate")['content'])
	ends = datetime.strftime(ends,'%m/%d/%Y')

	row = {"Companies": company, "Shows": show, "Starts": starts, "Ends": ends, "Attendees": "N/A", "Exhibitors": "N/A"}

	# Attendees
	try:
		for data in soup.find_all('div', class_='events-numbers'):
			if data.p.text == 'Attendees':
				attendees = data.span.text
				row["Attendees"] = attendees
	except Exception, e:
		pass

	# Exhibitors
	try:
		for data in soup.find_all('div', class_='events-numbers'):
			if data.p.text == 'Exhibiting Companies':
				exhibitors = data.span.text
				row["Exhibitors"] = exhibitors
	except Exception, e:
		pass

	# Decode any unicode formatting
	row = convert(row)
	print 'Processing page: ', event_url
	return row

entries = 0
with open('eventegg-list.csv', 'w') as csvfile:
	w = csv.DictWriter(csvfile, fieldnames=fieldnames)
	w.writeheader()

	while page <= 284:
		openurl = urlopen(base + nextpage + str(page))
		soup = BeautifulSoup(openurl, 'html.parser')
		for data in soup.find_all('a', class_='view-details-btn'):
			row = get_event_info(data.get('href'))
			w.writerow(row)
			entries += 1
			print 'Processed: ', entries
		page += 1

print 'Done..'



